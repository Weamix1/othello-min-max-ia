import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# import graphe
# g = graphe.lire_france()
# g.edges
# g.positions

# Nombre de départements + 1

sz = 96

# Graphe plan
# Le champ g.edges a le type "graphe" standard du fichier 
# dijkstra.py

class Graphe2D:

    def __init__(self, n):
        self.positions = n * [(0,0)]
        self.edges = n * [None]
        self.names = n * ['']
        for i in range(n): self.edges[i] = []

    # Distance dans le plan entre le ième et le jème sommet du
    # graphe.
    # Le facteur 1.9 convertit à peu près les pixels en kilomètres

    def distance(self, i, j):
        (x, y) = self.positions[i]
        (a, b) = self.positions[j]
        dx = x - a
        dy = y - b
        return 1.9 * math.sqrt(dx * dx + dy * dy)

# Lecture dans le fichier routes.dat des routes entre les villes
# voisines de France

def lire_routes(g):
    villes = open('routes.dat')
    for i in range(1, sz):
        s = villes.readline().split(';')
        for j in range(1, len(s)):
            try: 
                x = int(s[j])
                g.edges[i].append((x, g.distance(x, i)))
            except: pass

# Lecture dans le fichier villes.dat des coordonnées des villes
# de France

def lire_villes(g):
    villes = open('villes.dat')
    for i in range(1, sz):
        s = villes.readline().split(';')
        g.names[i] = s[1]
        try: 
            x = int(s[2])
            y = int(s[3])
            g.positions[i] = (x, y)
        except: pass

# Graphe plan des préfectures des départements français

def lire_france():
    g = Graphe2D(sz)
    lire_villes(g)
    lire_routes(g)
    return g

# Affichage de la carte de France avec villes et routes

def afficher_carte():
    carte = mpimg.imread('france.png')
    plt.imshow(carte)
    g = lire_france()
    for i in range(1, sz):
        x, y = g.positions[i]
        s = g.edges[i]
        for (j, p) in s:
            a, b = g.positions[j]
            plt.plot([x, a], [550-y, 550-b], color='black', lw=1)

# Voir la carte de France avec villes et routes

def voir_carte():
    afficher_carte()
    plt.show()

# Voir une liste de chemins entre des couples de villes

def voir_chemin_aux(g, c):
    for i in range(len(c) - 1):
        x, y = g.positions[c[i]]
        a, b = g.positions[c[i + 1]]
        plt.plot([x, a], [550-y, 550-b], color='red', lw=2)

def voir_chemins(g, cs):
    afficher_carte()
    for c in cs: 
        voir_chemin_aux(g, c)
    plt.show()

def TrueNode(g):
    E = {}
    for index, edge in enumerate(g, start=0): 
        if len(edge) != 0:
            E[index]= edge
    return E

def Dijkstra(g,nd,na):
    d = {}
    prec = {}
    E = TrueNode(g.edges)
    for a in E: #a une ville parmi les nodes
        d[a] = np.infty #dist plus petite pour aller nd à a
        prec[a] = None #point par lequel on doit passer pour le plus petit chemin partant de nd allant jusqu'à a et qui est connu jusque là
    d[nd] = 0
    while E != None:
        #tu peux faire une boucle sur E tout simplement, et garder comme u le noeud a qui minimise d[a] : u devrait ê égal à départ?
        #la ville qui minimise 
        #tu cherches le a où d[a] est le plus petit
        u = np.infty
        min = np.infty
        for a in E:
            if(len(E[a]) != 0):
                if d[a] < min:
                    min = d[a]
                    u = a 

        if u == na:
            return(d,prec)
        currentNode = E[u]
        del E[u]

        for v in currentNode:
            alt = d[u] + v[1]
            if(alt < d[v[0]]):
                d[v[0]]=alt
                prec[v[0]]=u

def AStar(g,nd,na):
    E = TrueNode(g.edges)
    E.update({nd: E[nd]})
    d = {}
    f = {}
    prec = {}
    for a in E :
        d[a] = f[a] = np.infty
        prec[a] = None
    d[nd] = 0
    hnd = g.distance(nd, na)
    f[nd]= hnd
    while E != None:
        u = np.infty
        min = np.infty
        for a in E:
            if len(E.get(a)) != 0:
                if f[a] < min:
                    min = f[a]
                    u = a 

        if u == na:
            return(d,prec)

        if u!= np.infty:
            currentNode = E[u]
            del E[u]

            for v in currentNode:
                alt = d[u] + v[1]
                if(alt < d[v[0]]):
                    d[v[0]]=alt
                    hv = g.distance(nd, v[0])
                    f[v[0]] = d[v[0]] + hv
                    prec[v[0]]=u
                    if v[0] not in E:
                        E.update(v[0])