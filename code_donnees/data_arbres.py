import numpy as np

class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)
        
    def __repr__(self):
        return 'x: '+str(self.x)+', y: '+str(self.y)

def load_data(filelocation):
    with open(filelocation,'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x,y,attributs))
    return data

class Noeud:
    def __init__(self, profondeur_max=np.infty, hauteur=0):
        self.question = None
        self.enfants = {}
        self.profondeur_max = profondeur_max
        self.proba = None
        self.hauteur = hauteur

    def prediction(self, d):
        current = self
        while current.profondeur_max > 0 and len(current.enfants) > 0:
            current = current.enfants[question_inf(d.x, current.question[0], current.question[1])]
        return current.proba
        
    def grow(self,data):
        entrop = entropie(data)
        best_question = best_split(data)
        proba_emp = proba_empirique(data)
        if(best_question is not None):
            d = split(data,best_question[0],best_question[1])
            if entrop > 0 and self.profondeur_max !=0 and len(d[0])>=1 and len (d[1])>=1:
                self.question = best_question
                enfant1=Noeud(self.profondeur_max-1, self.hauteur+1)
                self.enfants[True] = enfant1
                enfant2=Noeud(self.profondeur_max-1, self.hauteur+1)
                self.enfants[False] = enfant2
                enfant1.grow(d[0])
                enfant2.grow(d[1])
        self.proba = proba_emp

def proba_empirique(d):
    res = {0:0,1:0}
    for data in d:
        y = data.y
        if y in res:
            res[y] +=1 / len(d)
        else :
            res[y] = 1 / len(d)
    return res

def question_inf(x,a,s):
    if x[a]<s:
        return True
    else:
        return False

def split(data,a,s):
    d1=[]
    d2=[]
    for d in data:
        if question_inf(d.x,a,s) == True:
            d1.append(d)
        else:
            d2.append(d)
    return d1,d2
    
def list_separ_attributs(data, a):
    l = []
    separ = []
    for d in data:
        l.append(d.x[a])
    l = list(set(l))
    l.sort()
    for i in range(len(l)-1):
        separ.append((l[i] + l[i+1])/2)
    return [(a, s) for s in np.sort(separ)]
   
def liste_questions(d):
    res = []
    for a in d[0].x:
        res += list_separ_attributs(d, a)
    return res

def entropie(d):
    d = proba_empirique(d)
    #print (d)
    h=0
    for key,value in d.items():
        valeurlog = np.log(value) if value !=0 else 0
        h=h+(value*valeurlog)
        #print(h)
    h=-h
    return h

def gain_entropie(d,a,s):
    d1,d2=split(d,a,s)
    r1=len(d1)/len(d)
    r2=len(d2)/len(d)
    h=entropie(d)
    h1=entropie(d1)
    h2=entropie(d2)
    g=h-(r1*h1)-(r2*h2)
    return g

def best_split(d):
    best_question = None
    max_gain_entropie = -np.infty
    for a,s in liste_questions(d):
        gain = gain_entropie(d,a,s)
        if gain > max_gain_entropie:
            max_gain_entropie = gain
            best_question = (a,s)
    return best_question

def precision(noeud, ld):
    nb_prediction_ok = 0
    for d in ld:
        predic= noeud.prediction(d)
        if((d.y == 1 and predic[1] >= predic[0]) or (d.y == 0 and predic[0] >= predic[1])):
            nb_prediction_ok += 1
    return nb_prediction_ok / len(ld)