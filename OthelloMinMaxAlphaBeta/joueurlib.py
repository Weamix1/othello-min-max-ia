import interfacelib
import numpy as np
import time

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interfacelib.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):
	
	def demande_coup(self):
		#pass  #A remplacer par ce qui va bien
		liste = self.jeu.plateau.liste_coups_valides(self.couleurval)
		return liste[np.random.randint(0,len(liste))]

class Minmax(IA):
	compteurJouer = 0

	def demande_coup(self):
		start = time.time()
		print("MinMax :")
		print("La methode jouer est utilisée ",self.compteurJouer," fois." )
		print ("Durée : ",(time.time() - start)*1000,"ms")
		return self.min_max(self.jeu.plateau,3,self.couleurval)[1] #profondeur 3 par défaut

	def score(self,plateau):
		noir = 0
		blanc = 0
		for i in range(plateau.taille):
			for j in range(plateau.taille):
				if plateau.tableau_cases[i][j] == 1:
					noir += 1
				if plateau.tableau_cases[i][j] == -1:
					blanc += 1
		if(self.couleurval ==1): 
			score_val = noir
		else:
			score_val = blanc
		return score_val

	def min_max(self,plateau,profondeur,couleur):
		if profondeur==0:
			return (self.score(plateau),None)
		if couleur==self.couleurval : #maximisation
			score_normal = -np.infty
			liste = self.jeu.plateau.liste_coups_valides(couleur)
			for coup_valide in liste:
				copie_plateau = plateau.copie()
				copie_plateau.jouer(coup_valide,couleur)
				self.compteurJouer+=1
				res = self.min_max(copie_plateau,profondeur-1,-couleur)
				score_copie = res[0]
				if score_normal < score_copie:
					score_normal = score_copie
					meilleur_coup = coup_valide
			return (score_normal,meilleur_coup)
		else : #minimisation
			score_normal = np.infty
			liste = self.jeu.plateau.liste_coups_valides(couleur)
			for coup_valide in liste:
				copie_plateau = plateau.copie()
				copie_plateau.jouer(coup_valide,couleur)
				self.compteurJouer+=1
				res = self.min_max(copie_plateau,profondeur-1,-couleur)
				score_copie = res[0]
				if score_normal > score_copie:
					score_normal = score_copie
					meilleur_coup = coup_valide
			return (score_normal,meilleur_coup)

class AlphaBeta(IA):

	compteurJouer = 0

	def demande_coup(self):
		start = time.time()
		print("AlphaBeta :")
		print("La methode jouer est utilisée ",self.compteurJouer," fois." )
		print ("Durée : ",(time.time() - start)*1000,"ms")
		return self.alpha_beta(self.jeu.plateau,3,self.couleurval,-np.infty,np.infty)[1] #profondeur 3 par défaut

	def score(self,plateau):
		noir = 0
		blanc = 0
		for i in range(plateau.taille):
			for j in range(plateau.taille):
				if plateau.tableau_cases[i][j] == 1:
					noir += 1
				if plateau.tableau_cases[i][j] == -1:
					blanc += 1
		if(self.couleurval ==1): 
			score_val = noir
		else:
			score_val = blanc
		return score_val

	def alpha_beta(self,plateau,profondeur,couleur,alpha,beta):
		if profondeur==0:
			return (self.score(plateau),None)
		if couleur==self.couleurval :
			score_normal = -np.infty
			liste = self.jeu.plateau.liste_coups_valides(couleur)
			for coup_valide in liste:
				copie_plateau = plateau.copie()
				copie_plateau.jouer(coup_valide,couleur)
				self.compteurJouer+=1
				res = self.alpha_beta(copie_plateau,profondeur-1,-couleur,alpha,beta)[0]
				score_copie = res
				if score_normal < score_copie:
					score_normal = score_copie
					meilleur_coup = coup_valide
					alpha = max(alpha,score_normal)
				if alpha >= beta:
					break
			return (score_normal,meilleur_coup)
		else :
			score_normal = np.infty
			liste = self.jeu.plateau.liste_coups_valides(couleur)
			for coup_valide in liste:
				copie_plateau = plateau.copie()
				copie_plateau.jouer(coup_valide,couleur)
				self.compteurJouer+=1
				res = self.alpha_beta(copie_plateau,profondeur-1,-couleur,alpha,beta)[0]
				score_copie = res
				if score_normal > score_copie:
					score_normal = score_copie
					meilleur_coup = coup_valide
					beta = max(beta,score_normal)
				if alpha >= beta:
					break
			return (score_normal,meilleur_coup)
