# Vitse Maxime (apprenti)

## Informations TP :

### TP1 :  Othello Min/Max AlphaBeta

- Random
- MinMax (profondeur paramètrable)
- AlphaBeta (profondeur paramètrable)

Profondeur paramétré par défaut : 3

Compteurs de temps + compteurs de fois où la méthode jouer est appelée

For execution (see run_othello.ipynb)

### TP2 :  Exercice_graphes (Djikstra/A*)

#### Djikstra

```ipynb
import graphe
g = graphe.lire_france()
var = graphe.Dijkstra(graphe.lire_france(), 1, 10)
print(var[0])
print(var[1])
```

#### A*

```ipynb
import graphe
g = graphe.lire_france()
var = graphe.Dijkstra(graphe.lire_france(), 1, 10)
print(var[0])
print(var[1])
```

For execution (see run_graphes.ipynb)

### TP3 :  Code_donnees

For execution (see run_data_arbres.ipynb)